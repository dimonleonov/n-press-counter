from tkinter import *
from tkinter import ttk # TTK reference, kekw
from threading import Thread
import time
import keyboard


class counter:
    # считалка и перехватывалка нажатий на клаву
    def __init__(self, label):
        self.counter = 0
        self.widget = label
       
    def event(self, wtf):
        # вызывается если юзер ткнул на N
        self.counter += 1
        self.widget.config(text = 'N была нажата '+str(self.counter)+' раз')
    
    def run(self):
        # собсна гря, делаем красоту
        self.widget.config(text = 'N была нажата '+str(self.counter)+' раз')
        keyboard.on_release_key("n", self.event)
        
class window: 
    # окошечко
    def __init__(self):
        self.root = Tk()
        self.root.title("N counter")
        self.root.geometry("250x50")
 
        self.label = ttk.Label(text="Тут я покажу, сколько раз ты нажал на N")
        self.label.pack()
  
    def run(self):
        self.root.mainloop()
    


w = window()
c = counter(w.label)
ct = Thread(target=c.run, args=[]) # листенер в отдельный поток
ct.start()
w.run()