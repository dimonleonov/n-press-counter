# N Press Counter

Это небольшая программа для моего друга, который попросил сделать софт, который бы считал, сколько раз он нажал на клавиатуре кнопку "N".

# Скачать
[Тык на переход](https://gitlab.com/dimonleonov/n-press-counter/-/blob/main/dist/N_press_counter.exe)

# Что нужно поставить для самостоятельной сборки
1) python (желательно 3)
2) модуль keyboard
```
pip3 install keyboard
```